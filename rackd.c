#include "rackd.h"

#include <stdio.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <limits.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>

char *racksdir = "/tmp/racks/";
size_t racksdir_len = 0;

char *udspath = "/tmp/rackd.sckt";
size_t udspath_len = 0;

bool cstr_eq(const char *a, const char *b){
	for (int i = 0;; i++){
		if (a[i] != b[i]) return false;
		else if (a[i] == '\0') return true;
	}
}

bool has_backdir(const char *path){
	enum { BEGIN, NORMAL, DOT1, DOT2 } state = BEGIN;

	for (char c; (c = *path) != '\0'; path++){
		if (c == '.'){
			if (state == BEGIN) state = DOT1;
			else if (state == DOT1) state = DOT2;
			else state = NORMAL;
		} else if (c == '/'){
			if (state == DOT2) break;
			else state = BEGIN;
		} else state = NORMAL;
	}

	return state == DOT2;
}

char **racks = NULL;
size_t racks_count;

int index_racks(){
	if (racks){
		for (unsigned i = 0; i < racks_count; i++) free(racks[i]);
		free(racks);
	}

	/* if (true) */{
		DIR *rdh = opendir(racksdir);
		if (!rdh) return errno;

		racks_count = 0;
		struct dirent *dir;
		while ((dir = readdir(rdh)) != NULL){
			if (dir->d_type == DT_DIR && dir->d_name[0] != '.') racks_count++;
		}

		if (closedir(rdh) == -1) return errno;
	}

	racks = malloc(sizeof(char *) * racks_count);

	int error = 0;
	for (unsigned i = 0; i < racks_count && !error; i++){
		char *manifest_path;
		asprintf(&manifest_path, "%s/%u/rack", racksdir, i);

		int manifest = open(manifest_path, O_RDONLY);
		if (manifest != -1){
			char *name = malloc(64);
			ssize_t len = read(manifest, name, 63);
			if (len == -1){
				free(name);
				error = errno;
				racks_count = i;
			} else {
				name = realloc(name, len + 1);
				name[len] = '\0';
				close(manifest);
				racks[i] = name;
			}
		} else {
			error = errno;
			racks_count = i;
		}

		free(manifest_path);
	}

	return error;
}

#define SERVER   's'
#define LOADER   'l'
#define MEDIATOR 'm'
#define HELP     'h'

char mode = HELP;

void rackd(char *forkmode, char *last_arg /* Can be NULL */){
	if (fork() == 0){
		execlp(BINARY_PATH, BINARY_PATH, "-r", racksdir, "-u", udspath, forkmode, last_arg, (char *)NULL);
		printf("rackd-%c: failed to spawn rackd%s (%s)\n", mode, forkmode, strerror(errno));
		exit(1);
	}
}

int loader_main();
int server_main(int mediator_pid);
int mediator_main(void);

int main(int argc, char *argv[]){
	char *lastarg = NULL;

	prctl(PR_SET_PDEATHSIG, SIGHUP);

	for (char *arg; (arg = *argv); argv++){
		/**/ if (arg[0] != '-') lastarg = arg;
		else if (arg[1] == 'r') racksdir = *(++argv);
		else if (arg[1] == 'u') udspath = *(++argv);
		else if (arg[1] == 's') mode = SERVER;
		else if (arg[1] == 'l') mode = LOADER;
		else if (arg[1] == 'm') mode = MEDIATOR;
		else if (arg[1] == 'h') mode = HELP;
	}

	if (udspath)  udspath_len  = strlen(udspath);
	if (racksdir) racksdir_len = strlen(racksdir);

	int e = 0;
	printf("rackd-%c: spawned\n", mode);

	/**/ if (mode == HELP)         printf("usage: rackd (-r [racksdir] -m) | -h\n");
	else if (!racksdir)            printf("Missing racksdir\n");
	else if (mode == MEDIATOR) e = mediator_main();
	else if (!udspath)             printf("Missing udspath\n");
	else if (mode == LOADER)   e = loader_main();
	else if (!lastarg)             printf("Missing mpid\n");
	else if (mode == SERVER)   e = server_main(atoi(lastarg));

	if (e) printf("rackd-%c → %s\n", mode, strerror(e));

	return 0;
}