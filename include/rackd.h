/* SPDX-License-Identifier: GPL-2.0+ */
#include <unistd.h>
#include <limits.h>
#include <stdbool.h>
#include <errno.h>

extern void rackd(char *mode, char *last_arg);

extern bool cstr_eq(const char *a, const char *b);

extern bool has_backdir(const char *path);

extern int index_racks();

extern char **racks;
extern size_t racks_count;

extern char *racksdir;
extern size_t racksdir_len;

extern char *udspath;
extern size_t udspath_len;