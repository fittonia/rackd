// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * loader / entry point + setup + websocket loop
 *
 */

#include "rackd.h"
#include "shared_object.h"
#include "ws_frames.h"
#include "recv_fd.h"
#include "messages.h"

#include <sys/ptrace.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

int wsfd = -1;
unsigned rack_id;

void signal_handler(int sig){
	printf("rackd-l: Rack crashed (s:%i)\n", sig);
	// close
	static ws_frame_t frame = {
		.fin = true,
		.opcode = 0x8,
		.length = 0,
		.payload = NULL,
	};
	if (wsfd >= 0) websockets_send_frame(wsfd, &frame);
	exit(1);
}

int loader_main(){

	// this channel is trusted
	int error = retrieve_rack_info(&wsfd, udspath, &rack_id);

	if (error == 0){
		printf("rackd-l: loading rack #%u\n", rack_id);
		char *rack_root;
		asprintf(&rack_root, "%s/%u/back/", racksdir, rack_id);
		error = (chdir(rack_root) == -1) ? errno : 0;
		free(rack_root);
	}

	// if (error == 0){
	// 	int fd = open("/sys/kernel/security/hypnos/register", O_WRONLY);
	// 	if (fd == -1) error = errno;
	// 	else {
	// 		error = write(fd, &rack_id, sizeof(rack_id)) == -1 ? errno : 0;
	// 		close(fd);
	// 	}
	// }

	if (error == 0){
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		signal(SIGSEGV, signal_handler);
	}

	while (error == 0){
		ws_frame_t frame = { .fin = true };
		error = websockets_recv_frame(wsfd, &frame);
		if (error == 0){
			if (frame.opcode == 0x8) break;
			else if (frame.opcode == 0x9) frame.opcode = 0xA;
			else if (frame.fin) error = process_message(&frame);
			websockets_send_frame(wsfd, &frame);
			usleep(100);
		}
	}

	if (error == 0){
		printf("rackd-l: #%u shutting down normally\n", rack_id);
	} else {
		printf("rackd-l: #%u errored\n", rack_id);
	}
	close(wsfd);
	return error;
}