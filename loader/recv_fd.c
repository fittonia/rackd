// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * loader / reception of the socket file descriptor from server
 *
 */

#include "recv_fd.h"

#include <sys/select.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/un.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

// https://stackoverflow.com/a/2358843/8270240
ssize_t recv_fd(int uds, void *ptr, size_t nbytes, int *recvfd){
	struct msghdr msg;

	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	struct iovec iov;
	iov.iov_base = ptr;
	iov.iov_len = nbytes;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	ssize_t n;
	if ((n = recvmsg(uds, &msg, 0)) > 0){
		if ((cmptr = CMSG_FIRSTHDR(&msg)) != NULL && cmptr->cmsg_len == CMSG_LEN(sizeof(int))){
			if (cmptr->cmsg_level != SOL_SOCKET) printf("control level != SOL_SOCKET");
			else if (cmptr->cmsg_type != SCM_RIGHTS) printf("control type != SCM_RIGHTS");
			else {
				int *data = (int *)CMSG_DATA(cmptr);
				*recvfd = *data;
			}
		} else *recvfd = -1; // descriptor was not passed
	}

	return n;
}

int retrieve_rack_info(int *wsfd, char *socket_path, unsigned *rack_id){
	int cfd = socket(AF_UNIX, SOCK_STREAM, 0);
	int error = (cfd == -1) ? errno : 0;

	if (error == 0){
		struct sockaddr_un addr;
		memset(&addr, 0, sizeof(addr));
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);
		int n = connect(cfd, (struct sockaddr*)&addr, sizeof(addr));
		if (n == -1) error = errno;
	}

	if (error == 0){
		ssize_t n = recv_fd(cfd, rack_id, sizeof(*rack_id), wsfd);
		if (n < 0) error = errno;
	}

	return error;
}