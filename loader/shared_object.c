// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * loader / LibDL wrappers
 *
 */

#include "shared_object.h"

#include <dlfcn.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>

#define SO_HANDLES 20
void *handles[SO_HANDLES] = { NULL };

void abort_ldr(char *origin, char *msg){
	printf("rackd-l: Fatal error (%s): %s\n", origin, msg);
	raise(SIGABRT);
}

unsigned load_lib(char *name){
	unsigned h = SO_HANDLES;
	for (unsigned i = 0; i < SO_HANDLES; i++){
		if (handles[i] == NULL){
			h = i;
			break;
		}
	}

	if (h < SO_HANDLES){
		char *path;
		asprintf(&path, "./%s", name);
		void *handle = dlopen(path, RTLD_NOW);
		free(path);
		if (handle) handles[h] = handle;
		else abort_ldr("libdl", dlerror());
	} else abort_ldr("loader", "Too much handles");

	return h;
}

char *call_lib_function(unsigned h, char *name, char *args){
	char *ret;
	void *handle = handles[h];
	if (h < SO_HANDLES && handle){
		callable_symbol_t symbol = dlsym(handle, name);
		if (symbol){
			ret = symbol(args);
			if (!ret) ret = "[nullptr returned]";
		} else abort_ldr("libdl", dlerror());
	} else abort_ldr("loader", "Invalid handle");

	return ret;
}