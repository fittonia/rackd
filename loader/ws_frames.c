// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * loader / websocket server protocol (frames)
 *
 */

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "ws_frames.h"

int recv_bytes(int cfd, uint64_t *data, size_t bytes){
	uint8_t tmp[8] = { 0 };
	uint64_t _data = 0;
	if (recv(cfd, tmp, bytes, 0) < 1) return errno;
	for (unsigned i = 0; i < bytes; i++){
		_data <<= 8;
		_data |= tmp[i];
	}
	*data = _data;
	return 0;
}

int send_bytes(int cfd, uint64_t data, size_t bytes){
	uint8_t tmp[8] = { 0 };
	for (unsigned i = 0; i < bytes; i++){
		tmp[i] |= (data & 0xff);
		data >>= 8;
	}
	return (write(cfd, tmp, bytes) == -1) ? errno : 0;
}

int websockets_recv_frame(int cfd, ws_frame_t *frame){
	uint8_t header[2];
	if (recv(cfd, &header, sizeof(header), 0) < 1) return errno;

	ws_frame_t nf;

	nf.fin = header[0] >> 7;
	nf.opcode = header[0] & 0x0f;
	uint8_t masked_payload = header[1] >> 7;
	uint8_t pl_len = header[1] & 0x7f;

	int error = 0;
	uint64_t length = 0;
	if (pl_len < 126) length |= pl_len;
	else error = recv_bytes(cfd, &length, (pl_len == 126) ? 2 : 8);

	uint64_t masking_key = 0;
	if (error == 0 && masked_payload) error = recv_bytes(cfd, &masking_key, 4);

	if (error == 0 && length){
		uint8_t *payload = malloc(length);
		nf.dynalloc = true;
		nf.payload = payload;
		nf.length = length;
		while (length){
			size_t n = recv(cfd, payload, length, 0);
			if (n < 1){
				error = errno;
				free(payload);
			}
			payload += n;
			length -= n;
		}
		length = nf.length;
	}

	if (error == 0 && masked_payload){
		uint8_t j = 24;
		for (uint64_t i = 0; i < length; i++){
			uint8_t mb = masking_key >> j;
			nf.payload[i] = nf.payload[i] ^ mb;
			j = (j == 0) ? 24 : (j - 8);
		}
	}

	if (error == 0){
		if (nf.opcode == 0 && frame->fin == false){
			frame->length += nf.length;
			frame->payload = realloc(frame->payload, frame->length);
			memcpy(frame->payload + frame->length, nf.payload, nf.length);
			frame->fin = nf.fin;
		} else {
			if (frame->payload && frame->dynalloc) free(frame->payload);
			*frame = nf;
		}
	}

	return error;
}

int websockets_send_frame(int cfd, ws_frame_t *frame){
	uint64_t length = frame->length;
	uint8_t pl_len = (length < 126) ? length : ((length < 65536) ? 126 : 127);

	int error = 0;
	if (true){
		// no mask for server → client messages
		uint8_t header[2] = { (frame->opcode & 0x0f), pl_len };
		header[0] |= (frame->fin) ? 0x80 : 0;
		error = (write(cfd, header, sizeof(header)) == -1) ? errno : 0;
	}

	if (error == 0 && pl_len >= 126){
		error = send_bytes(cfd, length, (pl_len == 126) ? 2 : 8);
	}

	if (error == 0){
		error = (write(cfd, frame->payload, length) == -1) ? errno : 0;
	}

	return error;
}