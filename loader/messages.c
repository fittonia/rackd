// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * loader / decoding of websocket messages
 *
 */

#include "rackd.h"
#include "messages.h"
#include "shared_object.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int load_library_message(ws_frame_t *frame){
	char *retval;
	char *so_path = (char *)frame->payload;
	int n, handle = -1;

	if (!has_backdir(so_path)){
		printf("rackd-l: loading %s\n", so_path);
		char buffer[30];
		handle = load_lib(so_path);
		n = sprintf(buffer, "%u", handle);
		retval = buffer;

		// retval = gen_js_interface_for_rack(so_path, NULL);
	} else abort_ldr("load_lib_msg", "Invalid SO path");

	frame->payload = realloc(frame->payload, n);
	memcpy(frame->payload, retval, n);
	frame->length = n;

	// if (handle >= 0) free_gen_js(retval);

	return 0;
}

#define read_item(variable, op)  \
	variable = (char *)payload;  \
	while (*payload) payload++;  \
	if (payload++ op limit) abort_ldr("call_fn_msg", "Invalid message");
//

const char *item_error = "[Invalid payload]";

int call_function_message(ws_frame_t *frame){
	uint8_t *payload = frame->payload;
	uint8_t *limit = payload + frame->length;
	const char *retval = NULL;
	unsigned h;

	char *h_str, *fn_str, *args;
	read_item(h_str, ==)
	read_item(fn_str, ==)
	read_item(args, !=)
	
	h = atoi(h_str);

	retval = call_lib_function(h, fn_str, args);

	int n = strlen(retval);
	payload = malloc(n);
	memcpy(payload, retval, n);
	free(frame->payload);
	frame->payload = payload;
	frame->length = n;

	return 0;
}

size_t shift_nts(uint8_t *s, size_t len){
	for (size_t i = 0, j = 1; j < len; i = j++){
		s[i] = s[j];
	}
	s[--len] = '\0';
	return len;
}

int process_message(ws_frame_t *frame){
	uint64_t length = frame->length;
	uint8_t *payload = frame->payload;
	int error = 0;
	if (length){
		uint8_t code = payload[0];
		frame->length = shift_nts(payload, length);
		/**/ if (code == 'L') error = load_library_message (frame);
		else if (code == 'C') error = call_function_message(frame);
		else {
			char msg[20];
			sprintf(msg, "Unkown operation: %c", code);
			abort_ldr("process_msg", msg);
		}
	}
	return error;
}