/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef WS_FRAMES_H
#define WS_FRAMES_H

#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct ws_frame {
	uint64_t length;
	uint8_t *payload;
	uint8_t opcode;
	bool fin;
	bool dynalloc;
} ws_frame_t;

int recv_bytes(int cfd, uint64_t *data, size_t bytes);

int send_bytes(int cfd, uint64_t data, size_t bytes);

int websockets_recv_frame(int cfd, ws_frame_t *frame);

int websockets_send_frame(int cfd, ws_frame_t *frame);

#endif