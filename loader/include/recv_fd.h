/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef RECV_FD_H
#define RECV_FD_H

#include <unistd.h>

ssize_t recv_fd(int uds, void *ptr, size_t nbytes, int *recvfd);

int retrieve_rack_info(int *wsfd, char *socket_path, unsigned *rack_id);

#endif