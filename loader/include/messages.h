/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef MESSAGES_H
#define MESSAGES_H

#include "ws_frames.h"

int load_lib_message(ws_frame_t *frame);

int call_function_message(ws_frame_t *frame);

int process_message(ws_frame_t *frame);

#endif