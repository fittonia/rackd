/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef SHARED_OBJECT_H
#define SHARED_OBJECT_H

typedef char *(*callable_symbol_t)(char *);

void abort_ldr(char *origin, char *msg);

unsigned load_lib(char *name);

char *call_lib_function(unsigned h, char *name, char *args);

#endif