// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * server / websocket server protocol (handshake)
 *
 */

#include <errno.h>
#include "kcapi.h"
#include "b64.h"
#include "http.h"
#include "ws_handshake.h"

#define SHA_DIGEST_LENGTH 20
char ws_key[] = "___sec_key_goes_here____258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

struct kcapi_handle *handle;

void init_crypto(){
	kcapi_md_init(&handle, "sha1", 0);
}

int ws_upgrade_from_http(int cfd, char *sec_key){
	for (unsigned i = 0; i < 24; i++){
		char _c = sec_key[i];
		if (_c == '\0') return -2;
		ws_key[i] = _c;
	}
	if (sec_key[24] != '\0') return -3;

	uint8_t sha1[SHA_DIGEST_LENGTH];
	kcapi_md_digest(handle, (const uint8_t *)ws_key, 60, sha1, SHA_DIGEST_LENGTH);

	char result[28];
	b64encode_ob(sha1, SHA_DIGEST_LENGTH, result);

	char *data[] = {
		"HTTP/1.1 101 Switching Protocols\r\n" // data[0]
		"Upgrade: websocket\r\n"               // data[0]
		"Connection: Upgrade\r\n"              // data[0]
		"Sec-WebSocket-Accept: ",              // data[0]
		result,                                // data[1]
		"\r\n\r\n"                             // data[2]
	};
	size_t len[] = { 97, 28, 4 };
	return write_successive(cfd, data, len, 3);
}