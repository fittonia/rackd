// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * server / entry point + socket server
 *
 */

#include "ws_handshake.h"
#include "load_rack.h"
#include "rackd.h"
#include "http.h"

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "b64.h"

int srv(int port){
	struct sockaddr_in addr_in;
	addr_in.sin_family = AF_INET;
	addr_in.sin_port = htons(port);
	addr_in.sin_addr.s_addr = INADDR_ANY;

	memset(addr_in.sin_zero, '\0', sizeof(addr_in.sin_zero));
	struct sockaddr *addr = (struct sockaddr *)&addr_in;

	int listen_queue = 10;

	int sfd = socket(AF_INET, SOCK_STREAM, 0);
	int error = (sfd != -1) ? 0 : errno;

	if (!error){
		int optval = 1;
		setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
		if (bind(sfd, addr, sizeof(addr_in)) == -1) error = errno;
	}

	if (!error){
		if (listen(sfd, listen_queue) == -1) error = errno;
	}

	while (!error){
		struct sockaddr_storage client;
		socklen_t sz = sizeof(client);

		int cfd = accept(sfd, (struct sockaddr *)&client, &sz);
		error = (cfd == -1) ? errno : handle_http(cfd);
	}

	return error;
}

int server_main(int mediator_pid){
	init_crypto();
	get_mempage_config();
	int error = init_rack_loader(mediator_pid);
	if (!error) error = index_racks();
	if (!error) error = (chdir(racksdir) == -1) ? errno : srv(1337);
	return error;
}