// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * server / base64 decoding / encoding
 *
 */

#include <stdlib.h>
#include <stdint.h>

#include "b64.h"

uint8_t btoa[128] =
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	"\0\0\0\0\0\0\0\0\0\0\0\x3e\0\0\0\x3f"
	"\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\0\0\0\0\0\0"
	"\0\x0\x1\x2\x3\x4\x5\x6\x7\x8\x9\xa\xb\xc\xd\xe"
	"\xf\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\0\0\0\0\0"
	"\0\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28"
	"\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\0\0\0\0\0";
int8_t atob[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void b64encode_ob(uint8_t *bytes, size_t len, char *output){
	uint8_t a, b, c, _b, _c;
	for (size_t j = 0, i = 0; i < len;){
		a = bytes[i++];
		_b = i < len;
		b = _b ? bytes[i++] : 0;
		_c = i < len;
		c = _c ? bytes[i++] : 0;

		output[j++] = atob[a >> 2];
		output[j++] = atob[(b >> 4) | ((a & 3) << 4)];
		output[j++] = _b ? atob[((b & 15) << 2) | (c >> 6)] : '=';
		output[j++] = _c ? atob[c & 63] : '=';
	}
}

char *b64encode(uint8_t *bytes, size_t len, size_t *olen){
	size_t _olen = ((len / 3) + (len % 3 != 0)) * 4;
	char *output = malloc(_olen);
	b64encode_ob(bytes, len, output);
	*olen = _olen;
	return output;
}

uint8_t *b64decode(char *b64, size_t len, size_t *olen){
	uint8_t *_b64 = (uint8_t *)b64;
	uint8_t a, b, c, d, _c, _d;
	size_t _olen = ((len / 4) + (len % 4 != 0)) * 3;
	uint8_t *output = malloc(_olen);
	for (size_t j = 0, i = 0; i + 1 < len;){
		a = btoa[_b64[i++] & 127];
		b = btoa[_b64[i++] & 127];
		_c = (i < len) ? _b64[i++] : '=';
		_d = (i < len) ? _b64[i++] : '=';
		c = btoa[_c & 127];
		d = btoa[_d & 127];

		output[j++] = (a << 2) | (b >> 4);
		output[j++] = (b << 4) | ((c >> 2) & 15);
		output[j++] = (c << 6) | (d & 63);

		uint8_t sub = (_c == '=') + (_d == '=');
		_olen -= sub;
		if (sub) break;
	}
	*olen = _olen;
	return output;
}