/* SPDX-License-Identifier: GPL-2.0+ */
#include <unistd.h>
#include <limits.h>

int init_rack_loader(int themis_pid);

ssize_t send_fd(int uds, void *ptr, size_t nbytes, int sendfd);

int load_rack(int wsfd, unsigned rack_id);