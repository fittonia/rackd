/* SPDX-License-Identifier: GPL-2.0+ */
#include <unistd.h>
#include <stdint.h>

void b64encode_ob(uint8_t *bytes, size_t len, char *output);

char *b64encode(uint8_t *bytes, size_t len, size_t *olen);

uint8_t *b64decode(char *b64, size_t len, size_t *olen);