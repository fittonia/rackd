/* SPDX-License-Identifier: GPL-2.0+ */
#include <unistd.h>
#include <stdbool.h>

typedef struct request_line {
	char *method;
	char *path;
	char *version;
} request_line_t;

bool str4_eq(const char *a, char *b);

bool str2_eq(const char *a, char *b);

int write_successive(int cfd, char **data, size_t *len, unsigned count);

int write_response(int cfd, char *status, size_t slen, char *response, size_t rlen);

char **http_split_lines(char *r, size_t sz, unsigned *hlines);

void http_parse_headers(char **lines, unsigned hlines);

request_line_t http_parse_request_line(char *req_line);

void get_mempage_config();

int serve_file(int cfd, char *rpath);

int handle_get_request(int cfd, char **lines, unsigned hlines, char *path);

int handle_http(int cfd);