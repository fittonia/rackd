/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef WS_HANDSHAKE_H
#define WS_HANDSHAKE_H

extern void init_crypto();

extern int ws_upgrade_from_http(int cfd, char *sec_key);

#endif