// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * server / http protocol + file serving
 *
 */

#include "ws_handshake.h"
#include "load_rack.h"
#include "rackd.h"
#include "http.h"

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>

#define HTTP_PREFIX "HTTP/1.1 "
#define HTTP_PREFIX_LEN 9

#define HC200 "200 OK"
#define HC302 "302 Found"
#define HC404 "404 Not found"
#define HC405 "405 Method Not Allowed"
#define HC421 "421 Misdirected Request"

#define HC(n) (HC##n), (sizeof(HC##n) - 1)
#define write_error(cfd, n) write_response(cfd, HC(n), "<h1>Error: " HC##n "</h1>", (sizeof(HC##n) + 15))

bool str4_eq(const char *a, char *b){
	return (*(uint32_t *)a) == (*(uint32_t *)b);
}

bool str2_eq(const char *a, char *b){
	return (*(uint16_t *)a) == (*(uint16_t *)b);
}

int write_successive(int cfd, char **data, size_t *len, unsigned count){
	int error = 0;
	while (count-- && error == 0){
		if (write(cfd, *data, *len) == -1) error = errno;
		data++; len++;
	}
	return error;
}

int write_response(int cfd, char *status, size_t slen, char *response, size_t rlen){
	char len_str[20] = { 0 };
	int n = sprintf(len_str, "%lu", rlen);
	char *data[] = { HTTP_PREFIX, status, "\r\nContent-Length: ", len_str, "\r\n\r\n", response };
	size_t len[] = { HTTP_PREFIX_LEN, slen, 18, n, 4, rlen };
	return write_successive(cfd, data, len, 6);
}

int write_redirect(int cfd, char *new_url, size_t url_len){
	char *data[] = { HTTP_PREFIX, HC302, "\r\nConnection: close\r\nLocation: ", new_url, "\r\n\r\n" };
	size_t len[] = { HTTP_PREFIX_LEN, sizeof(HC302) - 1, 31, url_len, 4 };
	return write_successive(cfd, data, len, 5);
}

char **http_split_lines(char *r, size_t sz, unsigned *hlines){
	unsigned _hlines = 0;
	for (size_t i = 0; i < sz; i++){
		if (str2_eq("\r\n", r + i)) _hlines++;
	}
	char **lines = malloc(sizeof(char *) * _hlines * 2); // enough room for both field and value
	unsigned hidx = 0;
	size_t last_line = 0;
	for (size_t i = 0; i < sz; i++){
		if (str2_eq("\r\n", r + i)){
			lines[hidx++] = r + last_line;
			last_line = i + 2;
			r[i] = '\0';
		}
	}
	*hlines = _hlines;
	return lines;
}

void http_parse_headers(char **lines, unsigned hlines){
	// skip request line
	for (size_t i = 1; i < hlines; i++){
		char *line = lines[i];
		for (size_t j = 0; line[j]; j++){
			if (line[j] == ':'){
				lines[i + hlines] = line + j + (line[j + 1] == ' ' ? 2 : 1);
				line[j] = '\0';
				break;
			}
		}
	}
}

request_line_t http_parse_request_line(char *req_line){
	request_line_t rl = { req_line, NULL, NULL };
	for (unsigned i = 0; req_line[i]; i++){
		if (req_line[i] == ' '){
			req_line[i] = '\0';
			if (rl.path){
				rl.version = req_line + i + 1;
				break;
			} else rl.path = req_line + i + 1;
		}
	}
	return rl;
}

size_t page_size;
void get_mempage_config(){
	page_size = sysconf(_SC_PAGE_SIZE);
}

int serve_file(int cfd, char *file_path){
	int fd = open(file_path, O_RDONLY);

	int error;
	char *file_chunk;
	if (fd != -1){
		file_chunk = mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
		error = 0;
	} else error = errno;

	if (!error){
		// keeping this for consistency (could have been one string)
		char *data[] = { HTTP_PREFIX, HC200, "\r\nConnection: close\r\n\r\n" };
		size_t len[] = { HTTP_PREFIX_LEN, sizeof(HC200) - 1, 23 };
		error = write_successive(cfd, data, len, 3);
	}

	while (!error){
		ssize_t sz = read(fd, file_chunk, page_size);
		if (sz == 0) break;
		else if (sz == -1 || write(cfd, file_chunk, sz) == -1) error = errno;
	}

	if (fd != -1){
		close(fd);
		munmap(file_chunk, page_size);
	}
	return error;
}

bool parse_host(char *host, unsigned *rack_id, char **rack_name, size_t *rack_len){

	char *h = host;
	bool is_rid = (*h == 'r');

	if (is_rid){
		for (char c; (c = *++h); ){
			if (c == '.'){
				++h;
				break;
			} else if (is_rid && ('0' > c || c > '9')) is_rid = false;
		}
	}

	if (is_rid) *rack_id = atoi(host);
	else h = host;

	*rack_name = h;
	unsigned len = 0;
	for (char c; (c = *h); ++h){
		if (c == '.') break;
		else len++;
	}
	*rack_len = len;

	return is_rid;
}

int redirect_to_rid(int cfd, char *rack_name, size_t rack_len, char *host, char *path){
	unsigned rack_id;

	int error = index_racks();
	if (!error){
		error = 2;
		for (unsigned i = 0; i < racks_count; i++){
			if (strncmp(racks[i], rack_name, rack_len) == 0){
				rack_id = i;
				error = 0;
				break;
			}
		}
	}

	if (!error){
		char *new_url;
		ssize_t url_len = asprintf(&new_url, "http://r%u.%s%s", rack_id, host, path);
		error = write_redirect(cfd, new_url, url_len);
		free(new_url);
	}

	return error;
}

int handle_get_request(int cfd, char **lines, unsigned hlines, char *path){

	http_parse_headers(lines, hlines);

	char *host = NULL;
	char *ws_key = NULL;

	for (unsigned i = 1; i < hlines; i++){
		if (cstr_eq("Host", lines[i])){
			host = lines[i + hlines];
		} else if (cstr_eq("Sec-WebSocket-Key", lines[i])){
			ws_key = lines[i + hlines];
		}
	}

	char tmp[] = "r0.rustrack.rack";
	host = tmp; // for test purpose

	int error;
	if (host){
		char *status = "Error";

		unsigned rack_id;
		char *rack_name;
		size_t rack_len;
		bool has_rid = parse_host(host, &rack_id, &rack_name, &rack_len);

		if (!has_rid){
			error = redirect_to_rid(cfd, rack_name, rack_len, host, path);
			status = "Redirect";
		} else if (rack_id >= racks_count || strncmp(rack_name, racks[rack_id], rack_len) != 0){
			error = write_error(cfd, 421);
			status = "Mismatch";
		} else if (ws_key){
			error = ws_upgrade_from_http(cfd, ws_key);
			if (error == 0) error = load_rack(cfd, rack_id);
			if (error == 0) status = "Rack";
		} else {
			char *file_path;
			if (has_backdir(path)) error = 2;
			else if (asprintf(&file_path, "%u/front/%s", rack_id, path) != -1){
				error = serve_file(cfd, file_path);
				free(file_path);
			} else error = 2;
			if (!error) status = "File";
		}

		if (error == 2){
			error = write_error(cfd, 404);
			status = "Denied / Not Found";
		}

		printf("rackd-s: %s://%s%s → %s\n", ws_key ? "ws" : "http", host, path, status);
	}

	return error;
}

int handle_http(int cfd){
	size_t sz = 0;
	char *r = NULL;
	bool found = false;
	while (!found){
		int pending;
		ioctl(cfd, FIONREAD, &pending);
		if (pending){

			r = realloc(r, sz + pending);

			int result = recv(cfd, r + sz, pending, 0);
			if (result <= 0) break;
			else sz += result;

			for (size_t i = 0; (i + 3) < sz && !found; i++){
				found = str4_eq("\r\n\r\n", r + i);
				if (found) sz = i + 2;
			}

		} else usleep(100);
	}

	unsigned hlines;
	char **lines = http_split_lines(r, sz, &hlines);
	request_line_t rl = http_parse_request_line(lines[0]);

	int error = 0;
	if (cstr_eq("GET", rl.method)) error = handle_get_request(cfd, lines, hlines, rl.path);
	else error = write_error(cfd, 405);

	free(lines);
	free(r);

	close(cfd);

	return error;
}