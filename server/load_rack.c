// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * server / socket file descriptor server + themis signal
 *
 */

#include "load_rack.h"
#include "rackd.h"

#include <sys/socket.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>

int themis_pid;
int ipc_socket;

int init_rack_loader(int tpid){
	themis_pid = tpid;

	unlink(udspath);

	ipc_socket = socket(AF_UNIX, SOCK_STREAM, 0);
	if (ipc_socket == -1) return errno;

	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, udspath, sizeof(addr.sun_path) - 1);

	int n = bind(ipc_socket, (struct sockaddr*)&addr, sizeof(addr));
	if (n == -1) return errno;

	return (listen(ipc_socket, 1) == -1) ? errno : 0;
}

int load_rack(int wsfd, unsigned rack_id){
	sigqueue(themis_pid, SIGRTMIN, (union sigval){ .sival_int = 0 });

	int cfd = accept(ipc_socket, NULL, NULL);
	if (cfd == -1) return errno;

	if (send_fd(cfd, &rack_id, sizeof(rack_id), wsfd) == -1) return errno;
	else return (close(cfd) == -1) ? errno : 0;
}

// https://stackoverflow.com/a/2358843/8270240
ssize_t send_fd(int uds, void *ptr, size_t nbytes, int sendfd){
	struct msghdr msg;

	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	cmptr = CMSG_FIRSTHDR(&msg);
	cmptr->cmsg_len = CMSG_LEN(sizeof(int));
	cmptr->cmsg_level = SOL_SOCKET;
	cmptr->cmsg_type = SCM_RIGHTS;
	int *data = (int *)CMSG_DATA(cmptr);
	*data = sendfd;

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	struct iovec iov;
	iov.iov_base = ptr;
	iov.iov_len = nbytes;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	return sendmsg(uds, &msg, 0);
}