 * [Goal](#goal)
 * [Dependencies](#dependencies)
 * [Installation instructions](#installation-instructions)
 * [How to run](#how-to-run)
 * [How it works](#how-it-works)
 * [Illustration](#rackd-illustrated)

# rackd - allowing controlled OS access to webapps

## Goal

The goal of this project is to allow webapps running in a web browser to use native code.

Such native code:
 * is run outside of the browser
 * is sandboxed (by the 'Hypnos' Linux Security Module)
 * can use system calls / functions like file and process management
 * takes the form of shared libraries

## Dependencies

To parse JSON, the mediator has its own copy of [jsmn](https://zserge.com/jsmn/) sitting in `mediator/include/jsmn.h`.

### Kernel

 * Hypnos support (currently disabled, uncomment `if` block in `loader/main.c:main` to enable)
 * Kernel cryptography APIs (SHA1 / userland)

### Userland

 * libkcapi
 * libc
 * libdl

## Installation instructions

If `make` complains about missing symbols, in Makefile.am, change: `rackd_LDFLAGS = -lkcapi` to `rackd_LDFLAGS = -lkcapi -ldl`.

To proceed with the installation:
```sh
autoreconf -i
./configure
make
sudo make install
# And optionnally:
sudo make uninstall
```

## How to run

```sh
tar -xf racks.tar.xz -C /tmp
rackd -r /tmp/racks/ -m
```

If everything went right, you should be able to get a webpage at http://localhost:1337/rustrack.html

## To do
 * store running racks in the mediator
 * if there is already a rack process running, spawn a thread inside instead of a new process
 * mutlthread the http server
 * provide an external (ipc) interface to permissions infringements
 * (to be decided) separate compiler from mediator
 * provide v4l2loopback interfaces to racks
 * backend: fake stdout and pipe it to console.log in frontend

## How it works

This projects builds as one executable file, `rackd`, which embeds 3 programs.

### Mediator

The mediator is a controller:
 * it compiles and updates apps permissions for hypnos
 * it spawns an HTTP+WS server queried by the web browser
 * upon receiving a signal from the server, it spawns loader processes
 * it acts as a ptracer for the loader it spawns, noticed by hypnos if apps commit permissions infringements

### Loader

The loader acts as a host for webapps backends:
 * it is ptraced by the mediator
 * it is sandboxed by the 'hypnos' LSM
 * it loads the backends (shared objects under `[rack]/back/`) using libdl
 * it is used to call backend functions from the frontend
 * it receives orders from the frontend (web browser / webapp js), through a websocket session

### Server

The server was written to be dead simple, only adding websockets capabilities to the base HTTP protocol.
 * it serves files under `[rack]/front/`.
 * on websocket requests, it signals the mediator which will spawn a new loader. The websocket session is handed to the loader via a unix domain socket once the handshake is done.

## rackd illustrated

![interactions](https://assets.gitlab-static.net/uploads/-/system/project/avatar/22199010/rackd.png)
