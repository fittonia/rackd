// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * mediator / upload of new permissions files to Hypnos
 *
 */

#include "parser.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define HOOKS_COUNT 2;

#define RACK_HOOKS_SIZE(racks) (racks * HOOKS_COUNT)

char *read_whole_file(const char *path){
	FILE * f = fopen(path, "r");
	if (f == NULL) return NULL;
	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	char * string = malloc(fsize + 1);
	fread(string, 1, fsize, f);
	fclose(f);
	string[fsize] = 0;
	return string;
}

int write_one_shot(const char *path, void *data, size_t len){
	int fd = open(path, O_WRONLY);
	if (fd < 0) return errno;
	int c = write(fd, data, len);
	close(fd);
	return c;
}



const char *apply_json_update(const char *json_filter_file){
	char *json = read_whole_file(json_filter_file);
	const char *error;
	if (json){
		error = parse_filter(json);
		if (error == NULL){
			void *update;
			unsigned size;
			error = compile(&update, &size);
			if (error == NULL){
				int c = write_one_shot("/sys/kernel/security/hypnos/update", update, size);
				printf("rackd-m: hypnos updated (%s)\n", strerror(c));
				free(update);
			}
		}
		free(json);
	} else error = "json file read";
	return error;
}