/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef THEMIS_H
#define THEMIS_H

const char *apply_json_update(const char *json_filter_file);

#endif