/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef TRACER_H
#define TRACER_H

#include <sys/types.h>
#include <stdbool.h>
#include <limits.h>
#include <stdlib.h>
#include <dirent.h>
#include <signal.h>

extern void loop();

extern void setup_ptrace();

#endif