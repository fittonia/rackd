/* SPDX-License-Identifier: GPL-2.0+ */
#ifndef THEMIS_PARSER_H
#define THEMIS_PARSER_H

#include <stdbool.h>
#include <stdint.h>

typedef struct type type_t;
typedef struct inst inst_t;
typedef struct function function_t;

typedef enum {
	METHOD, // 1 + 2 argp bytes (.text) + args (.data)
	ACCEPT, // 1 byte
	REFUSE, // 1 byte
	CALLFN, // 1+2+6 bytes (code, addr, args)
	CALLRT, // 1 byte
} inst_type_t;

typedef struct cstr {
	char *array;
	unsigned len;
} cstr_t;

typedef struct name_and_type {
	const char *name;
	type_t *type;
} name_and_type_t;

typedef name_and_type_t function_param_t;

typedef struct name_and_hookp {
	const char *name;
	function_param_t *params;
} name_and_hookp_t;

typedef struct type_method_t {
	const char *name;
	type_t *arg_type;
	type_t *ret_type;
} type_method_t;

#define JZ_METHOD 0

typedef struct inst {
	inst_t *next;
	inst_type_t type;
	unsigned address;
	union {
		struct {
			unsigned method;
			type_method_t *method_p;
			int param;
			type_t *param_type;
			type_t *ret_type;
			union {
				int32_t arg_int;
				const char *arg_str;
			} arg;
		} method;
		struct {} accept;
		struct {} refuse;
		struct {
			uint16_t function;
			int8_t params[6];
		} callfn;
	} detail;
} inst_t;

typedef struct function {
	function_param_t args[6];
	inst_t *code;
	inst_t **next_loc;
	const char *id;
	unsigned hook;
	unsigned rack;
} function_t;

typedef struct type {
	const char *name;
	unsigned methods_len;
	type_method_t methods[];
} type_t;

typedef struct header {
	char sig[4];
	uint32_t racks_count;
	uint32_t hooks_count;
	uint32_t data_size;
	uint32_t functions_count;
} __attribute__ ((packed)) header_t;

typedef struct hook_header {
	uint16_t rack;
	uint16_t hook;
	uint32_t index;
} __attribute__ ((packed)) hook_header_t;


#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define GET(obj, key, dest) json_object_object_get_ex(obj, key, &(dest))

#define GET_OBJ_AS(obj, key, type, dest_obj)                 \
	if (!(GET(obj, key, dest_obj) && CHECK(dest_obj, type))) \
		return "Expected \"" key "\" (" #type ")";

#define GET_AS(obj, key, type, final)               \
{                                                   \
	json_object *dest_obj;                          \
	GET_OBJ_AS(obj, key, type, dest_obj)            \
	else final = json_object_get_##type(dest_obj);  \
}

const char *parse_filter(const char *json_text);

const char *compile(void **data, unsigned *len);

#endif