// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * mediator / entry point
 *
 */

#include "rackd.h"
#include "update_hypnos.h"
#include "tracer.h"

#include <sys/types.h>
#include <stdbool.h>
#include <dirent.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

int mediator_main(void){
	char pid[30];
	snprintf(pid, 30, "%u", getpid());

	rackd("-s", pid);
	const char *error = apply_json_update("/tmp/racks/test.json");
	if(!error){

		// ptrace
		printf("rackd-m: Listening for ptrace events\n");
		setup_ptrace();
		while (true) loop();

	} else printf("rackd-m: %s\n", error);

	return 1;
}