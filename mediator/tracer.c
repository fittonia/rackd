// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * mediator / exchanges with server + ptrace logic
 *          + spawning loader processes
 *
 */

#include "rackd.h"
#include "tracer.h"

#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>

void server_callback(int signum, siginfo_t *siginfo, void *context){
	rackd("-l", NULL);
}

void loop(){
	// char name[20];
	// fgets(name, sizeof(name), stdin);
	int stat_loc;
	pid_t pid = wait(&stat_loc);
	if (pid == -1) return;
	// printf("rackd-m: wait\n");
	if (stat_loc == SIGSTOP) stat_loc = 0;
	ptrace(PTRACE_CONT, pid, 0, stat_loc);
}

void setup_ptrace(){
	struct sigaction sa = { 0 };
	sigemptyset(&sa.sa_mask);
	sigaddset(&sa.sa_mask, SIGRTMIN);
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = server_callback;
	sigaction(SIGRTMIN, &sa, NULL);
}