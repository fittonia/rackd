// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Nathan Royer <royern@etu-univ-grenoble-alpes.fr>
 *
 * mediator / logic to transform json permissions into a binary format
 *
 */

#include "rackd.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define JSMN_PARENT_LINKS
#include "jsmn.h"
#include "parser.h"

const name_and_type_t none = { "", NULL };

type_t int_type = {
	"int", 2, {
		{ "jz", &int_type, NULL },
		{ "and", &int_type, &int_type }
	}
};

type_t string_type = {
	"string", 0, {}
};

type_t path_type = {
	"path", 2, {
		{ "under", &string_type, &int_type },
		{ "equal", &string_type, &int_type }
	}
};

type_t file_type = {
	"file", 1, {
		{ "path", NULL, &path_type }
	}
};

type_t dentry_type = {
	"dentry", 0, {}
};

type_t *types[] = {
	&int_type,
	&string_type,
	&path_type,
	&file_type,
	&dentry_type,
};

function_param_t none_params[6] = {
	none, none, none, none, none, none
};

function_param_t no_params[6] = {
	none, none, none, none, none, none
};

function_param_t file_permission_params[6] = {
	{ "file", &file_type },
	{ "mask", &int_type },
	none, none, none, none
};

unsigned available_hooks_count = 3;
name_and_hookp_t available_hooks[] = {
	{ "path_rmdir", no_params },
	{ "path_mkdir", no_params },
	{ "file_permission", file_permission_params },
};

const char *inst_types[] = {
	"METHOD",
	"accept",
	"refuse",
	"CALLFN",
	"return",
};

const uint8_t inst_sizes[] = {
	3,
	1,
	1,
	9,
	1,
};

const int8_t inst_codes[] = {
	0,
	-3,
	-4,
	-1,
	-2,
};

unsigned f_count = 0;
function_t *functions = NULL;

const char *json_text;
jsmntok_t *tokens;
int tokens_count;

#define CHECK(i, _type) ((i != -1) && (tokens[(i)].type == (_type)))

static int find_next_child(int parent, int last_child){
	for (int i = last_child + 1; i < tokens_count; i++){
		if (tokens[i].parent == parent) return i;
	}
	return -1;
}

static int find_child(int parent, int last_child, const char *key, unsigned key_len){
	for (int i = last_child + 1; i < tokens_count; i++){
		jsmntok_t *t = tokens + i;
		int len = t->end - t->start;
		if (t->parent == parent && t->type == JSMN_STRING && key_len == len){
			if (strncmp(json_text + t->start, key, len) == 0) return i + 1;
		}
	}
	return -1;
}

static char *copy_cstr(const char *src, unsigned len){
	char *dst = malloc(len + 1);
	for (unsigned i = 0; i < len; i++) dst[i] = src[i];
	dst[len] = '\0';
	return dst;
}

static char *copy_jstr(int i){
	jsmntok_t t = tokens[i];
	int len = t.end - t.start;
	return copy_cstr(json_text + t.start, len);
}

static int get_jint(int i){
	char *s = copy_jstr(i);
	int r = atoi(s);
	free(s);
	return r;
}

static const char *parse_method(int inst_obj, inst_t *inst, function_t *current, inst_t *prev){

	int obj_name_tok = find_child(inst_obj, -1, "object", 6);
	if (!CHECK(obj_name_tok, JSMN_STRING)) return "Expected method object (string).";
	char *object_name = copy_jstr(obj_name_tok);

	type_t *class = NULL;
	if (cstr_eq(object_name, "$") && prev && prev->type == METHOD){
		class = prev->detail.method.ret_type;
		inst->detail.method.param = -1;
	} else for (int i = 0; i < 6; i++){
		if (cstr_eq(object_name, current->args[i].name)){
			class = current->args[i].type;
			inst->detail.method.param = i;
			break;
		}
	}

	free(object_name);
	if (class == NULL) return "Unknown object.";

	int mth_name_tok = find_child(inst_obj, -1, "method", 6);
	if (!CHECK(mth_name_tok, JSMN_STRING)) return "Expected method name (string).";
	char *method_name = copy_jstr(mth_name_tok);

	type_t *arg_type = NULL, *ret_type = NULL;
	int method = -1;
	type_method_t *method_p = NULL;
	for (int i = 0; i < class->methods_len; i++){
		method_p = class->methods + i;
		if (cstr_eq(method_name, method_p->name)){
			arg_type = method_p->arg_type;
			ret_type = method_p->ret_type;
			method = i;
			break;
		}
	}

	free(method_name);
	if (method < 0) return "Unknown method.";

	inst->detail.method.method_p = method_p;
	inst->detail.method.method = method;
	inst->detail.method.param_type = class;
	inst->detail.method.ret_type = ret_type;

	int arg;
	if (arg_type == &int_type){
		arg = find_child(inst_obj, -1, "arg_num", 7);
		if (CHECK(arg, JSMN_PRIMITIVE)){
			inst->detail.method.arg.arg_int = get_jint(arg);
		} else return "Expected an integer in \"arg_num\".";
	} else if (arg_type == &string_type){
		arg = find_child(inst_obj, -1, "arg_str", 7);
		if (CHECK(arg, JSMN_STRING)){
			inst->detail.method.arg.arg_str = copy_jstr(arg);
		} else return "Expected a string in \"arg_str\".";
	}

	// TODO : impl a bank of symbol

	return NULL;
}

static const bool find_type(const char *name, function_param_t *arg){
	for (int i = 0; i < ARRAY_SIZE(types); i++){
		if (cstr_eq(name, types[i]->name)){
			arg->type = types[i];
			return true;
		}
	}
	return false;
}

static const char *parse_fn_params(int params, function_t *current){
	unsigned p_count = tokens[params].size;

	int last_key = -1;

	for (unsigned p = 0; p < p_count; p++){

		if (p >= 6){
			return "Too much parameters.";
		}

		last_key = find_next_child(params, last_key);
		current->args[p].name = copy_jstr(last_key);
		int type = find_next_child(last_key, -1);

		if (!CHECK(type, JSMN_STRING)){
			return "Invalid parameter specification.";
		}

		char *object_type = copy_jstr(type);
		bool success = find_type(object_type, &current->args[p++]);
		free(object_type);

		if (!success) return "Unkown type.";
	}

	return NULL;
}

static const char *parse_filter_fn_body(int body, function_t *current, bool hook, bool function);

static const char *parse_then(int inst_obj, inst_t *method_call, function_t *current){
	int then = find_child(inst_obj, -1, "then", 4);
	if (then == -1) return NULL;

	if (method_call->detail.method.ret_type != &int_type){
		return "Methods returning non-int cannot have a \"then\" property.";
	}

	inst_t *inst = malloc(sizeof(inst_t));
	*(current->next_loc) = inst;
	current->next_loc = &(inst->next);
	inst->next = NULL;

	inst->type = METHOD;
	inst->detail.method.param = -1; // prev (bool)
	inst->detail.method.param_type = &int_type;
	inst->detail.method.ret_type = NULL;
	inst->detail.method.method = JZ_METHOD; // (jmp if zero)
	inst->detail.method.method_p = int_type.methods;

	const char *error = parse_filter_fn_body(then, current, false, false);
	if (error) return error;

	unsigned j = 0;
	for (inst_t *next = inst->next; next; j++) next = next->next;
	inst->detail.method.arg.arg_int = j;

	return NULL;
}

static const char *parse_callfn(int inst_obj, inst_t *inst, function_t *current){
	size_t offset = ((void *)current) - ((void *)functions);
	unsigned max_f = offset / sizeof(function_t);

	int it_tok = find_child(inst_obj, -1, "id", 2);
	if (!CHECK(it_tok, JSMN_STRING)) return "Expected callfn id (string).";

	char *id = copy_jstr(it_tok);

	function_t *function = NULL;
	unsigned f_index = 0;
	while (f_index < max_f){
		function_t *f = functions + f_index++;
		if (f->hook) break;
		if (cstr_eq(id, f->id)){
			function = f;
			f_index--;
			break;
		}
	}
	free(id);
	if (function == NULL) return "Unknown procedure.";

	inst->detail.callfn.function = f_index;

	int params = find_child(inst_obj, -1, "params", 6);
	if (!CHECK(params, JSMN_OBJECT)) return "Expected callfn params (object).";

	for (int i = 0; i < 6 && function->args[i].type; i++){
		const char *idx = function->args[i].name;
		int dest_tok = find_child(params, -1, idx, strlen(idx));

		if (CHECK(dest_tok, JSMN_STRING)){
			// find local variable
			int l = -1;

			char *local_param = copy_jstr(dest_tok);
			for (int j = 0; j < 6 && l < 0; j++){
				if (cstr_eq(local_param, current->args[j].name)){
					l = j;
					break;
				}
			}
			free(local_param);

			if (l < 0) return "Unknown local parameter.";

			if (function->args[i].type != current->args[l].type){
				return "Invalid type for parameter.";
			}

			inst->detail.callfn.params[i] = l;
		} else {
			return "Function call missing at least one parameter.";
		}
	}

	return NULL;
}

static const char *parse_filter_fn_body(int body, function_t *current, bool hook, bool function){
	if (!CHECK(body, JSMN_ARRAY)) return "Expected a body (array).";
	inst_t *prev = NULL;

	int inst_obj = -1;

	for (int i = 0; i < tokens[body].size; i++){
		
		inst_obj = find_next_child(body, inst_obj);
		if (i == 0 && function && !hook){
			int params = find_child(inst_obj, -1, "params", 6);
			if (!CHECK(body, JSMN_ARRAY)) return "Expected functions params spec.";
			const char *error = parse_fn_params(params, current);
			if (error) return error;
		} else {
			if (!CHECK(inst_obj, JSMN_OBJECT)) return "Expected an instruction (object).";

			int inst_type_tok = find_child(inst_obj, -1, "type", 4);
			if (!CHECK(inst_type_tok, JSMN_STRING)) return "Expected inst type (string).";

			char *inst_type = copy_jstr(inst_type_tok);

			inst_t *inst = malloc(sizeof(inst_t));
			*(current->next_loc) = inst;
			current->next_loc = &(inst->next);
			inst->next = NULL;

			int it = -1;
			/**/ if (cstr_eq(inst_type, "method")) it = 0;
			else if (cstr_eq(inst_type, "accept")) it = 1;
			else if (cstr_eq(inst_type, "refuse")) it = 2;
			else if (cstr_eq(inst_type, "callfn")) it = 3;

			free(inst_type);

			/**/ if (it == 0){
				inst->type = METHOD;
				const char *error = parse_method(inst_obj, inst, current, prev);
				if (error) return error;
				error = parse_then(inst_obj, inst, current);
				if (error) return error;
			} else if (it == 1){
				inst->type = ACCEPT;
			} else if (it == 2){
				inst->type = REFUSE;
			} else if (it == 3){
				inst->type = CALLFN;
				const char *error = parse_callfn(inst_obj, inst, current);
				if (error) return error;
			} else {
				return "Unknown instruction type.";
			}

			prev = inst;
		}
	}

	if (function){
		inst_t *ret_inst = malloc(sizeof(inst_t));
		*(current->next_loc) = ret_inst;
		current->next_loc = &(ret_inst->next);
		ret_inst->next = NULL;
		ret_inst->type = CALLRT;
	}

	return NULL;
}

static const char *translate_hook(function_t *current, size_t len){
	char *hook_name = (char *)current->id + len;
	*(hook_name++) = '\0';

	current->hook = 0;
	for (unsigned i = 0; i < available_hooks_count; i++){
		if (cstr_eq(hook_name, available_hooks[i].name)){
			current->hook = i + 1;
			break;
		}
	}
	if (current->hook == 0){
		return "Unknown hook name.";
	}

	current->rack = 0;
	for (unsigned i = 0; i < racks_count; i++){
		if (cstr_eq(current->id, racks[i])){
			current->rack = i + 1;
			break;
		}
	}
	if (current->rack-- == 0){
		return "Unknown rack name.";
	}

	return NULL;
}

static const char *parse_filter_functions(int root){
	f_count = tokens[root].size;

	if (functions) free(functions);
	functions = malloc(sizeof(function_t) * f_count);

	int last_key = -1;

	for (unsigned f = 0; f < f_count; f++){
		function_t *current = functions + f;
		last_key = find_next_child(root, last_key);
		current->id = copy_jstr(last_key);
		int body = find_next_child(last_key, -1);

		current->next_loc = &current->code;

		current->hook = 0;
		size_t len = 0;
		for (int i = 0; current->id[i] && !len; i++){
			if (current->id[i] == '.'){
				current->hook = 1;
				len = i;
			}
		}

		function_param_t *src;
		if (current->hook){
			const char *error = translate_hook(current, len);
			if (error) return error;
			src = available_hooks[current->hook - 1].params;
		} else {
			src = none_params;
		}
		for (int i = 0; i < 6; i++) current->args[i] = src[i];

		const char *error = parse_filter_fn_body(body, current, current->hook, true);
		if (error) return error;
	}

	return NULL;
}

const char *parse_filter(const char *json_text_p){
	if (index_racks()) return "Rack index error";

	json_text = json_text_p;
	int json_len = strlen(json_text);
	jsmn_parser parser = JSMN_PARSER_INIT;


	tokens_count = jsmn_parse(&parser, json_text, json_len, NULL, 0);
	tokens = malloc(tokens_count * sizeof(jsmntok_t));
	parser = (jsmn_parser)JSMN_PARSER_INIT;
	jsmn_parse(&parser, json_text, json_len, tokens, tokens_count);

	const char *error = tokens ? parse_filter_functions(0) : "Invalid JSON";

	free(tokens);
	return error;
}

void dis(){
	for (unsigned i = 0; i < f_count; i++){
		function_t *f = functions + i;
		if (f->hook){
			printf("hook [%u.%u] ", f->rack, f->hook - 1);
		} else {
			printf("proc %s ", f->id);
		}

		printf("(");
		for (unsigned j = 0; j < 6; j++){
			function_param_t *p = f->args + j;
			if (p->type == NULL) break;
			printf("%s %s, ", p->type->name, p->name);
		}
		printf("){\n");

		for (inst_t *inst = f->code; inst; inst = inst->next){
			if (inst->type == METHOD){
				int pi = inst->detail.method.param;
				type_method_t *m = inst->detail.method.method_p;

				printf("\t%s", pi < 0 ? "" : f->args[pi].name);
				printf(".%s(", m->name);
				if (m->arg_type == &int_type){
					printf("%i", inst->detail.method.arg.arg_int);
				} else if (m->arg_type == &string_type){
					printf("\"%s\"", inst->detail.method.arg.arg_str);
				}
				printf(")%c\n", inst->detail.method.ret_type ? 0 : ';');

			} else if (inst->type == CALLFN){
				unsigned fi = inst->detail.callfn.function;
				int8_t *actual = inst->detail.callfn.params;
				function_t *called = functions + fi;
				printf("\t%s(", called->id);
				for (unsigned j = 0; j < 6; j++){
					function_param_t *p = called->args + j;
					if (p->type == NULL) break;
					printf("%s = %s, ", p->name, f->args[actual[j]].name);
				}
				printf(");\n");
			} else printf("\t%s;\n", inst_types[inst->type]);
		}
		printf("}\n\n");
	}
}

const char *compile(void **data_p, unsigned *len_p){
	// dis();
	size_t data_len = 0, prog_len = 0, hooks_count = 0;
	for (unsigned i = 0; i < f_count; i++){
		function_t *f = functions + i;
		if (f->hook) hooks_count++;
		for (inst_t *inst = f->code; inst; inst = inst->next){
			prog_len += inst_sizes[inst->type];
			if (inst->type == METHOD){
				type_method_t *m = inst->detail.method.method_p;
				if (m->arg_type == &int_type){
					data_len += sizeof(int);
				} else if (m->arg_type == &string_type){
					data_len += strlen(inst->detail.method.arg.arg_str) + 1;
				}
			}
		}
	}

	size_t header_len = sizeof(header_t) + sizeof(hook_header_t) * hooks_count;
	header_t *header = malloc(header_len + data_len + prog_len);
	const char *sig = "XSF1";
	for (unsigned i = 0; i < 4; i++) header->sig[i] = sig[i];
	header->hooks_count = hooks_count;
	header->racks_count = racks_count;
	header->data_size = data_len;
	header->functions_count = f_count;
	hook_header_t *hook_header = (hook_header_t *)(header + 1);
	for (unsigned i = 0; i < f_count; i++){
		function_t *f = functions + i;
		if (f->hook){
			hook_header->rack = f->rack;
			hook_header->hook = f->hook - 1;
			hook_header->index = i;
			hook_header++;
		}
	}

	int8_t *data = (void *)header + header_len;
	int8_t *prog = data + data_len;
	size_t di = 0, pi = 0;
	for (unsigned i = 0; i < f_count; i++){
		function_t *f = functions + i;
		for (inst_t *inst = f->code; inst; inst = inst->next){
			if (inst->type == METHOD){

				// object + method
				int8_t c = inst->detail.method.param;
				if (c < 0 || c > 5) c = 7; // 0b111
				*(uint8_t *)&c |= inst->detail.method.method << 3;
				prog[pi++] = c;

				// data addr
				prog[pi++] = (int8_t)(di >> 8);
				prog[pi++] = (int8_t)(di & 0xff);

				// data
				type_method_t *m = inst->detail.method.method_p;
				if (m->arg_type == &int_type){
					int *dp = (int *)(data + di);
					*dp = inst->detail.method.arg.arg_int;
					di += sizeof(*dp);
				} else if (m->arg_type == &string_type){
					const char *src = inst->detail.method.arg.arg_str;
					do data[di++] = *src;
					while (*(src++));
				}
			} else prog[pi++] = inst_codes[inst->type];
			
			if (inst->type == CALLFN){
				uint16_t fn = inst->detail.callfn.function;
				int8_t *fnp = inst->detail.callfn.params;
				prog[pi++] = (int8_t)(fn & 0xff);
				prog[pi++] = (int8_t)(fn >> 8);
				for (unsigned j = 0; j < 6; j++) prog[pi++] = fnp[j];
			}
		}
	}

	if (data_len != di || prog_len != pi || (void *)hook_header != (void *)data){
		free(header);
		return "Compilation error: computed and actual program sizes differ\n";
	}

	*data_p = header;
	*len_p = header_len + data_len + prog_len;
	return NULL;
}